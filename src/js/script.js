$(document).ready(() => {

  $('.s-page-header__burger').on('click', function () {
    $('.s-mobile-menu').addClass('s-mobile-menu--is-active');
  });

  $('.s-mobile-menu__close').on('click', function () {
    $('.s-mobile-menu').removeClass('s-mobile-menu--is-active');
  });


  $('#leningrad').on({
    mouseenter: function () {
        $('.s-geography__label--neva').addClass('s-geography__label--is-active');
    },
    mouseleave: function () {
        $('.s-geography__label--neva').removeClass('s-geography__label--is-active');
    }
  });

  $('#krym').on({
    mouseenter: function () {
        $('.s-geography__label--water').addClass('s-geography__label--is-active');
    },
    mouseleave: function () {
        $('.s-geography__label--water').removeClass('s-geography__label--is-active');
    }
  });

  $('#zabaykalsky').on({
    mouseenter: function () {
        $('.s-geography__label--roads').addClass('s-geography__label--is-active');
    },
    mouseleave: function () {
        $('.s-geography__label--roads').removeClass('s-geography__label--is-active');
    }
  });

  $('#sakha').on({
    mouseenter: function () {
        $('.s-geography__label--strenth').addClass('s-geography__label--is-active');
    },
    mouseleave: function () {
        $('.s-geography__label--strenth').removeClass('s-geography__label--is-active');
    }
  });

  $('#kamchatka').on({
    mouseenter: function () {
      $('.s-geography__label--gold').addClass('s-geography__label--is-active');
    },
    mouseleave: function () {
      $('.s-geography__label--gold').removeClass('s-geography__label--is-active');
    }
  });

  $('#primorsky').on({
    mouseenter: function () {
      $('.s-geography__label--transoil').addClass('s-geography__label--is-active');
      $('.s-geography__label--rosoil').addClass('s-geography__label--is-active');
    },
    mouseleave: function () {
      $('.s-geography__label--transoil').removeClass('s-geography__label--is-active');
      $('.s-geography__label--rosoil').removeClass('s-geography__label--is-active');
    }
  });

});